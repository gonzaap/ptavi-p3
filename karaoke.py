#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import json
import urllib.request
from smallsmilhandler import SmallSMILHandler
from xml.sax import make_parser


class KaraokeLocal:

    def __init__(self, fichero):

        try:

            parser = make_parser()
            cHandler = SmallSMILHandler()
            parser.setContentHandler(cHandler)
            parser.parse(open(fichero))
            self.tags = cHandler.get_tags()
        except FileNotFoundError:
            sys.exit('No existe el archivo')

    def __str__(self):

        lista_next = ''
        for lista in self.tags:
            lista_next += lista[0]
            for elemento in lista[1]:
                if lista[1][elemento] != '':
                    lista_next += '\t' + elemento + '="'\
                                  + lista[1][elemento] + '"'
            lista_next += '\n'
        return lista_next

    def to_json(self, filesmil, filejson=''):

        if filejson == '':
            filejson = filesmil.replace('.smil', '.json')

        with open(filejson, 'w') as jsonfile:
            json.dump(self.tags, jsonfile, indent=3)

    def do_local(self):

        for lista in self.tags:
            for elemento in lista[1]:
                if elemento == 'src':
                    if lista[1][elemento].startswith('http'):
                        fichero = lista[1][elemento].split('/')[-1]
                        urllib.request.urlretrieve(lista[1][elemento], fichero)
                        lista[1][elemento] = fichero


if __name__ == '__main__':

    try:
        fichero = sys.argv[1]
    except IndexError:
        sys.exit('usage error: python3 karaoke.py file.smil')
    karaoke = KaraokeLocal(fichero)
    print(karaoke)
    karaoke.to_json(fichero)
    karaoke.do_local()
    karaoke.to_json(fichero, 'local.json')
    print(karaoke)
