#!/usr/bin/python3
# -*- coding: utf-8 -*-
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        self.lista = []

    def startElement(self, entrada, valor):

        if entrada == "root-layout":
            self.rootlayout = {}
            self.rootlayout["widht"] = str(valor.get("width", ""))
            self.rootlayout["height"] = str(valor.get("height", ""))
            self.rootlayout["background-color"] = \
                str(valor.get("background-color", ""))
            self.lista.append([entrada, self.rootlayout])

        elif entrada == "region":
            self.region = {}
            self.region["id"] = str(valor.get("id", ""))
            self.region["top"] = str(valor.get("top", ""))
            self.region["bottom"] = str(valor.get("bottom", ""))
            self.region["left"] = str(valor.get("left", ""))
            self.region["right"] = str(valor.get("right", ""))

        elif entrada == "img":

            self.img = {}
            self.img['src'] = str(valor.get("src", ""))
            self.img['region'] = str(valor.get("region", ""))
            self.img['begin'] = str(valor.get("begin", ""))
            self.img['dur'] = str(valor.get("dur", ""))
            self.lista.append([entrada, self.img])

        elif entrada == "audio":
            self.audio = {}
            self.audio['src'] = str(valor.get("src", ""))
            self.audio['begin'] = str(valor.get("begin", ""))
            self.audio['dur'] = str(valor.get("dur", ""))
            self.lista.append([entrada, self.audio])

        elif entrada == "textstream":
            self.textstream = {}
            self.textstream['src'] = str(valor.get("src", ""))
            self.textstream['region'] = str(valor.get("region", ""))
            self.lista.append([entrada, self.textstream])

    def get_tags(self):
        return self.lista


if __name__ == "__main__":

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())
